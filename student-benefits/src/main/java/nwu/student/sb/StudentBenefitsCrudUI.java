package nwu.student.sb;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ac.za.nwu.core.person.dto.PersonAffiliationInfo;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import nwu.student.sb.ui.service.PermissionProxyService;
import nwu.student.sb.ui.service.PersonProxyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.UI;

import nwu.ac.za.framework.SinglePageCRUDUI;
import nwu.ac.za.framework.VaadinUIUtility;
import nwu.ac.za.framework.context.SessionContext;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.sb.events.ClearErrorEvent;
import nwu.student.sb.events.ShowNotificationEvent;
import nwu.student.sb.ui.components.StudentBenefitsComponent;
import nwu.student.sb.ui.views.StudentBenefitsAdminView;
import nwu.student.sb.ui.config.ApplicationConfig;
import nwu.student.sb.ui.model.dto.ExecutionContext;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 * <p>
 * Created by Henriko on 2018/03/29.
 */
@Theme("nwu-default")
@SpringUI
public class StudentBenefitsCrudUI extends SinglePageCRUDUI {

    private ExecutionContext executionContext;
    private final Logger log = LoggerFactory.getLogger(StudentBenefitsCrudUI.class.getName());

	private boolean isStudent = false;

    @Autowired
    private ApplicationConfig appConfig;
    @Autowired
    private StudentBenefitsComponent nsfasAccomodationComponent;
    @Autowired
    private EventBus eventBus;
    @Autowired
    private VaadinI18n vaadinI18n;
    @Autowired
	private PersonProxyService personProxyService;
    @Autowired
    private PermissionProxyService permissionProxyService;
    @Autowired
    private StudentBenefitsAdminView studentBenefitsAdminView;

    @Override
    public String getBrowserTabTitle() {
        return "student-benefits";
    }

    @Override
    public AbstractLayout getDetailContent() {
        initialize();

        Label labelHeader = new Label(vaadinI18n.getMessage("slb.header.caption"));
        labelHeader.setStyleName("app-name");
        labelHeader.setHeight("42px");

        if(this.executionContext.isAdmin()){
            VerticalLayout tabVerticalLayout = new VerticalLayout();
            TabSheet tabs = new TabSheet();

            VerticalLayout studentTab = new VerticalLayout();
            studentTab.setCaption(vaadinI18n.getMessage("slb.tabs.student.label"));
            studentTab.addComponent(nsfasAccomodationComponent);
            nsfasAccomodationComponent.initUI(executionContext);

            VerticalLayout adminTab = new VerticalLayout();
            adminTab.setCaption(vaadinI18n.getMessage("slb.tabs.admin.label"));
            adminTab.addComponent(studentBenefitsAdminView);

            tabs.addComponents(studentTab, adminTab);
            tabs.addSelectedTabChangeListener((TabSheet.SelectedTabChangeListener) e -> {
                clearErrors();
                if(e.getTabSheet().getSelectedTab().getCaption().equals(vaadinI18n.getMessage("slb.tabs.admin.label"))){
                    studentBenefitsAdminView.gridYearDataCheck();
                }
            });

            tabVerticalLayout.addComponents(labelHeader, tabs);

            studentBenefitsAdminView.initTheView(executionContext);

            return tabVerticalLayout;
        } else {
            VerticalLayout layout = new VerticalLayout();
            nsfasAccomodationComponent.initUI(executionContext);
            layout.addComponents(labelHeader, nsfasAccomodationComponent);
            return layout;
        }
    }

    @Override
    public void afterUISetup() {
    }

    @Override
    public void save() {
    }

    @Override
    public SessionContext newSessionContext() {
        return new SessionContext();
    }

    @Subscribe
    public void clearUIErrors(ClearErrorEvent errorEvent) {
        this.clearErrors();
    }

    @Subscribe
    public void showNotification(ShowNotificationEvent event) {
        if (event.getTranslate()) {
            setNotificationMsg(event.getErrorKey(), event.getSeverityType(), true);
        } else {
            setNotificationMsg(event.getErrorMessage(), event.getSeverityType(), false);
        }
    }

	private void initialize() {
		lookupUser = this.authenticatedUser;
		
		setExecutionContextAndAuthentication();
		determineIsAdmin();
		determineIsStudent();
		eventBus.register(this);
	}

	private void determineIsAdmin() {
		
		boolean isAdmin = false;
		ArrayList<String> adminRoles = new ArrayList<>();
		adminRoles.add(appConfig.getAdminGroup());

		isAdmin = permissionProxyService.isUnivNumberInRoles(getSessionContext().getAuthenticatedUser(), adminRoles);
		this.executionContext.setAdmin(isAdmin);
		
	}

	public void determineIsStudent() {
		List<PersonAffiliationInfo> personAffiliationInfos = new ArrayList<>();
		personAffiliationInfos = personProxyService.getPersonAffiliation(executionContext.getLookupUser());

		if (personAffiliationInfos != null && personAffiliationInfos.size() > 0) {
			for (PersonAffiliationInfo userRole : personAffiliationInfos) {
				if (userRole.getAffiliationTypeKey().toLowerCase().contains("vss.code.hoedanig.s")) {
					isStudent = true;
				}
			}
		}

		if(!isStudent){
			log.error("University number: " + executionContext.getLookupUser() + " is not a student");
			throw new VaadinUIException(vaadinI18n.getMessage("uniNumber.not.student"));
		}
	}

    public void setExecutionContextAndAuthentication() {
        try {

            log.info("setExecutionContextAndAuthentication...");
            executionContext = new ExecutionContext();
            executionContext.setUserAuthenticated(true);
            executionContext.setLookupUser(lookupUser);
            executionContext.setAuthenticatedUser(lookupUser); // authenticatedUser
            // lookupUser
            Locale uiLocale = UI.getCurrent().getLocale();
            if (uiLocale.getLanguage().equals(VaadinUIUtility.SYSTEM_LANG_AF)) {
                executionContext.setSystemLanguageTypeKey(VaadinUIUtility.TYPE_KEY_SYSTEM_LANGUAGE_AF);
            } else {
                executionContext.setSystemLanguageTypeKey(VaadinUIUtility.TYPE_KEY_SYSTEM_LANGUAGE_EN);
            }

        } catch (Exception ex) {
            log.error("There was a problem with setting execution context: " + ex);
            throw new VaadinUIException("There was a problem with setting execution context: " + ex);
        }
    }

    public ExecutionContext getExecutionContext() {
        return executionContext;
    }

    public void setExecutionContext(ExecutionContext executionContext) {
        this.executionContext = executionContext;
    }

    public static StudentBenefitsCrudUI getCurrent() {
        return (StudentBenefitsCrudUI) UI.getCurrent();
    }

}
