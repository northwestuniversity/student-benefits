package nwu.student.sb.ui.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ac.za.nwu.core.type.dto.TypeAttributeValueInfo;
import ac.za.nwu.core.type.dto.TypeInfo;
import ac.za.nwu.core.type.dto.TypeNameInfo;
import ac.za.nwu.core.type.service.TypeService;
import ac.za.nwu.core.type.service.factory.TypeServiceClientFactory;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.sb.ui.config.Config;
import nwu.student.sb.ui.model.dto.TypeAttributeRequest;

@Service
public class TypeProxyService extends AbstractServiceProxy<TypeService> {

	@Autowired
	private Config config;

	@Autowired
	private KeyStoreManager keyStoreManager;

	@Override
	protected TypeService initService() throws Exception {

		String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getIdentityApiVersion());
		String appRuntimeEnv = config.getRuntimeEnvironment();

		String typeServiceKey = ServiceRegistryLookupUtility
				.getServiceRegistryLookupKey(TypeServiceClientFactory.TYPESERVICE, wsMajorVersion, null, appRuntimeEnv);
		System.out.println(typeServiceKey);
		try {
			String decryptedPassword = keyStoreManager.decrypt(config.getIApisReadPassword());

			return TypeServiceClientFactory.getTypeService(typeServiceKey, config.getIApisReadUsername(),
					decryptedPassword);
		} catch (Exception ex) {
			log.error("Could not initialize Type service: " + ex);
			throw new VaadinUIException("Could not initialize Type service.");
		}
	}

	public List<TypeAttributeValueInfo> getTypeAttributeValues(TypeAttributeRequest request) {
		try {

			List<TypeAttributeValueInfo> results = getService().getTypeAttributeValues(request.getTypeKey(),
					request.getCatagoryAttributeTypeKey(), getContextInfo(null));

			return results;

		} catch (DoesNotExistException e1) {
			log.warn("Could not get type attribute values. " + e1);
			// Ignoring the error
			return null;
		} catch (InvalidParameterException | OperationFailedException | MissingParameterException
				| PermissionDeniedException e) {

			log.error("Could not get type attribute values. " + e);
			throw new VaadinUIException("error.type.service.type.attribute.fail", true);
		}
	}

	public String getTypeLongDesc(String typeKey, String lang) {
		try {
			// We can remove the logging later I just added for debugging.
			TypeInfo respone = getService().getType(typeKey, getContextInfo(null));
			for (TypeNameInfo typeName : respone.getTypeNames()) {
				if (typeName.getLocale().contains("af") && lang.contains("2")) {
					return typeName.getLongDescr();
				}
				if (typeName.getLocale().contains("en") && lang.contains("3")) {
					return typeName.getLongDescr();
				}
			}

			String[] values = typeKey.split("\\.");
			return values[4]
					;
		} catch (DoesNotExistException | InvalidParameterException | OperationFailedException
				| MissingParameterException | PermissionDeniedException e) {
			log.error("Could not get type information from key: " + typeKey + " : " + e);
			throw new VaadinUIException("error.typeservice.gettype", true);
		}
	}

    public List<TypeInfo> getTypesByCategory(String typeKey, String language){
        try {
            return getService().getTypesByCategory(typeKey, language, getContextInfo(null));
        } catch (DoesNotExistException e) {
            throw new VaadinUIException("Type Service:getTypesByCategory Does not Exist",false);
        } catch (InvalidParameterException e) {
            throw new VaadinUIException("Type Service:getTypesByCategory Invalid Parameter",false);
        } catch (MissingParameterException e) {
            throw new VaadinUIException("Type Service:getTypesByCategory Missing Parameter",false);
        } catch (OperationFailedException e) {
            throw new VaadinUIException("Type Service:getTypesByCategory OperationFailed Exception",false);
        } catch (PermissionDeniedException e) {
            throw new VaadinUIException("Type Service:getTypesByCategory Permission Denied",false);
        }
    }
}
