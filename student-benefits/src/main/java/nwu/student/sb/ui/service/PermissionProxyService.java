package nwu.student.sb.ui.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.permission.service.PermissionService;
import nwu.enabling.capabilities.supporting.service.permission.service.factory.PermissionServiceClientFactory;
import nwu.student.sb.ui.config.ApplicationConfig;
import nwu.student.sb.ui.config.Config;

@Service
@RefreshScope
public class PermissionProxyService extends AbstractServiceProxy<PermissionService> {
    @Autowired
    private Config config;

    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Override
    protected PermissionService initService() throws Exception {

        try {
            String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getSupportingServicesVersion());
            String appRuntimeEnv = config.getRuntimeEnvironment();

            String permissionServiceLookupKey = getServiceRegistryLookupKey(
                    PermissionServiceClientFactory.PERMISSIONSERVICE,
                    wsMajorVersion + "/",
                    config.getWebServiceDatabase(),
                    appRuntimeEnv);

            log.info("Permission service lookupKey: " + permissionServiceLookupKey);
            String decryptedPassword = keyStoreManager.decrypt(config.getSupportingServicesReadPassword());

            return PermissionServiceClientFactory.getPermissionService(
                    permissionServiceLookupKey,
                    config.getSupportingServicesReadUsername(),
                    decryptedPassword);
        } catch (Exception e) {
            log.error("Could not initialize Permission service: " + e.getMessage(), e);
            throw new VaadinUIException("error.permission.service.init", e);
        }
    }

 
    public boolean isUnivNumberInRoles(String univNumber, List<String> roles) {
		
    	try {
			boolean result = getService().isUserInRoles(univNumber, roles, getContextInfo(null));
			return result;
        } catch (DoesNotExistException e) {
            log.error("Could not find members in group: " + e.getMessage());
            return false;

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen error when getting members from group: Reference number: 201001: " + e.getMessage());
            throw new VaadinUIException("service.permission.members.data.exception", true, e);

        } catch (Exception e) {
            log.error("Unforeseen exception when getting members from group: Reference number: 202001: " + e.getMessage());
            throw new VaadinUIException("service.permission.members.exception", true, e);
        }
	}
}
