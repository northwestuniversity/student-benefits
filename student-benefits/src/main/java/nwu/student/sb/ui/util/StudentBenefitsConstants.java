package nwu.student.sb.ui.util;

public class StudentBenefitsConstants {
	
    //Language
    public static final String LANGUAGE_AF = "af";

    //Property file values
    public static final String FAUDITSYSTEMFUNCTIONID = "144917";

	public static final String DIYLANDLORDOTHER = "vss.code.DIYLANDLORD.OTHER";
    //Language for catalogues
    public static final String MESSAGE_CATALOGUE_AF_NAME = "_af_ZA";
    public static final String MESSAGE_CATALOGUE_ENG_NAME = "_en_ZA";

    public static final String TYPE_KEY_PREFIX = "vss.code";
    public static final String AFFILIATION_INDEX = "HOEDANIG";

    //Business Validation Message Constants
    public static class BusinessValidation {
        public static final String VERSION_MISMATCH_ERROR = "version.mismatch.error";
        public static final String DUPLICATE_RECORD_ERROR = "duplicate.record.error";
    }

    //Missing parameter message constants
    public static class MissingParameter {
        public static final String VALUE_REQUIRED = "value.required";
    }

    //General validation message constants
    public static class GeneralValidation {
        public static final String CHARACTER_LENGTH_INVALID = "character.length.invalid";
        public static final String TYPE_KEY_FORMAT_INVALID = "type.key.format.invalid";
    }
    
    public static final String ONBASE_UNIVERSITY_KEYWORD = "ONBASE.CONFIG.KEYWORD.University Number.Universiteitsnommer";
    public static final String ONBASE_UPLOADED_BY_UNIVERSITY_KEYWORD = "ONBASE.CONFIG.KEYWORD.Uploaded by University Number.Universiteitsnommer";

}
