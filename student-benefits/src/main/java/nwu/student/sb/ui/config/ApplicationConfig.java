package nwu.student.sb.ui.config;

import java.util.Map;

import javax.annotation.PostConstruct;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.sb.ui.service.ConfigurationServiceProxy;

@Component
@RefreshScope
public class ApplicationConfig {
	private final String DIYPORTALLINK = "DIY.PORTAL.LINK";
	private final Logger log = LoggerFactory.getLogger(ApplicationConfig.class.getName());
	private final String CAS_ENABLED = "CAS.ENABLED";
	private final String CAS_SERVER_URL = "CAS.SERVER.URL";
	private final String ENVIRONMENT_URL = "ENVIRONMENT.URL";
	private final String WEB_CONTEXT = "WEB.CONTEXT";
	private final String DEV_INITIAL_UNIV_NUMBER = "DEV.INITIAL.UNIV.NUMBER";

	public final static String VERSION_GLOBAL = "GLOBAL";

	private final String ALLOW_UNIVERISTY_NUMBER_INPUT = "ALLOW.UNIVERSITY.NUMBER.INPUT";

	private final String Document_Service_User_Group = "DOCUMENT.SERVICE.USER.GROUP";
	public final String Document_Service_User_Password = "DOCUMENT.SERVICE.USER.PASSWORD";

	public final String ADMIN_GROUP = "ADMIN_GROUP";
	public final String ACADEMIC_YEAR = "ACADEMIC.YEAR";
	public final String MIN_AMOUNT_TO_BE_PAID = "MIN.AMOUNT.TO.BE.PAID.OUT";
	public final String SUBMIT_CONFIRM = "SUBMIT.CONFIRM";

	private static final String EMAILFROMUSERNAME_EMAILDISPLAYNAME = "EMAIL.FROMUSERNAME.EMAILDISPLAYNAME";
	private static final String EMAILREPLYTOADDRESS_EMAILADDRESS = "EMAIL.REPLYTOADDRESS.EMAILADDRESS";

	private final String DIY_ENG_URL = "DIY.SERVICE.URL";
	private final String DIY_AFR_URL = "DIY.AF.SERVICE.URL";

	@Autowired
	private ConfigurationServiceProxy configServiceProxy;

	@Autowired
	private Config config;

	private Map<String, String> appVersionConfiguration;
	private Map<String, String> appGlobalConfiguration;
	private String configPrefix;

	private String documentServiceUserGroup;
	private String documentServiceUserPassword;
	private String submitConfirm;

	private String fromUserNameEmailDisplayName;
	private String replyToAddressEmailAddress;

	@PostConstruct
	public void resetConfiguration() {
		appVersionConfiguration = configServiceProxy.getApllicationVersionSpecificConfiguration();
		appGlobalConfiguration = configServiceProxy.getApplicationGlobalConfiguration();
	}

	public String getCaseEnabled() {
		// If version has cas.enabled property return it, else return global setting
		return getPropertyValue(CAS_ENABLED);
	}

	public String getCASServerUrl() {
		return getPropertyValue(CAS_SERVER_URL);

	}

	public String getEnvironmentUrl() {
		return getPropertyValue(ENVIRONMENT_URL);
	}

	public String getWebContext() {
		return getPropertyValue(WEB_CONTEXT);
	}

	private String getPropertyValue(String propertyKey) {
		if (appVersionConfiguration == null || appGlobalConfiguration == null) {
			resetConfiguration();
		}

		String lookupVersionKey = "/" + getConfigPrefix() + "/V" + config.getAppMajorVersion() + "/" + propertyKey;
		String propertyValue = appVersionConfiguration.get(lookupVersionKey.toUpperCase());
		if (propertyValue != null) {
			return propertyValue;
		} else {
			String lookupGlobalKey = "/" + getConfigPrefix() + "/" + VERSION_GLOBAL + "/" + propertyKey;
			propertyValue = appGlobalConfiguration.get(lookupGlobalKey.toUpperCase());
			return propertyValue;
		}
	}

	public String getConfigPrefix() {
		if (configPrefix != null) {
			return configPrefix;
		}

		configPrefix = config.getRuntimeEnvironment() + "/" + config.getAppName();
		return configPrefix.toUpperCase();
	}

	/**
	 * Only return true if config exist and is equal to true, anything else will
	 * return false
	 */
	public String getDevInitialUnivNumber() {
		String propertyValue = getPropertyValue(DEV_INITIAL_UNIV_NUMBER);
		return propertyValue;

	}

	public String getDIY_PortalLink() {
		String propertyValue = getPropertyValue(DIYPORTALLINK);
		if (propertyValue == null) {
			throw new VaadinUIException("error.config.is.missing", true);
		}

		return propertyValue;

	}

	public Boolean getUniversitynumberAllowed() {
		String propertyValue = getPropertyValue(ALLOW_UNIVERISTY_NUMBER_INPUT);
		if (propertyValue == null) {
			log.error("Configuration for 'ALLOW.UNIVERSITY.NUMBER.INPUT' is missing - default to not allowed");
		} else if (propertyValue.trim().toLowerCase().equals("true")) {
			return true;
		}
		return false;
	}

	public String getDocumentServiceUserGroup() {
		if (Strings.isNullOrEmpty(documentServiceUserGroup)) {
			documentServiceUserGroup = getPropertyValue(Document_Service_User_Group);
		}
		return documentServiceUserGroup;
	}

	public String getDocumentServiceUserPassword() {
		if (Strings.isNullOrEmpty(documentServiceUserPassword)) {
			documentServiceUserPassword = getPropertyValue(Document_Service_User_Password);
		}
		return documentServiceUserPassword;
	}

	public String getAdminGroup() {
		String adminGroup = getPropertyValue(ADMIN_GROUP);

		return adminGroup;
	}

	public String getSubmitConfirm() {
		if(Strings.isNullOrEmpty(submitConfirm)) {
			submitConfirm = getPropertyValue(SUBMIT_CONFIRM);
		}
		return submitConfirm;
	}

	public String getFromUserNameEmailDisplayName() {
		if(Strings.isNullOrEmpty(fromUserNameEmailDisplayName)) {
			fromUserNameEmailDisplayName = getPropertyValue(EMAILFROMUSERNAME_EMAILDISPLAYNAME);
		}
		return fromUserNameEmailDisplayName;
	}

	public String getReplyToAddressEmailAddress() {
		if(Strings.isNullOrEmpty(replyToAddressEmailAddress)) {
			replyToAddressEmailAddress = getPropertyValue(EMAILREPLYTOADDRESS_EMAILADDRESS);
		}
		return replyToAddressEmailAddress;
	}

	public Integer getACADEMIC_YEAR() {
		String year = getPropertyValue(ACADEMIC_YEAR);
		if (year != null) {
			return Integer.parseInt(year);
		} else {
			return null;
		}
	}

	public Integer getMinAmountToBePaidOut() {
		String minAmountToBePaid = getPropertyValue(MIN_AMOUNT_TO_BE_PAID);
		if (minAmountToBePaid != null) {
			return Integer.parseInt(minAmountToBePaid);
		} else {
			log.error("Failed to retrieve minimum amount to be paid from ETCD");
			throw new VaadinUIException("error.config.is.missing", true);
		}
	}

	public String getDIY_UrlEN() {
		return getPropertyValue(DIY_ENG_URL);
	}

	public String getDIY_UrlAF() {
		return getPropertyValue(DIY_AFR_URL);
	}
}
