package nwu.student.sb.ui.service;

import ac.za.nwu.notification.dto.notifications.NotificationInfo;
import ac.za.nwu.notification.service.NotificationCompService;
import ac.za.nwu.notification.service.NotificationCompServiceNamespace;
import ac.za.nwu.registry.utility.GenericServiceClientFactory;
import assemble.edu.exceptions.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.sb.ui.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/*
 Created by Henriko on 2022/01/31
*/
@Service
public class NotificationCompProxyService extends AbstractServiceProxy<NotificationCompService> {

    @Autowired
    private Config config;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Override
    protected NotificationCompService initService() throws Exception {

        String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getNotificationAPIVersion());
        String appRuntimeEnv = config.getRuntimeEnvironment();

        String nofitificationCompServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                NotificationCompServiceNamespace.NOTIFICATION_NOTIFICATION_COMP_SERVICE_KEY,
                wsMajorVersion,
                config.getWebServiceDatabase(),
                appRuntimeEnv);

        log.info("LookupKey: " + nofitificationCompServiceLookupKey + "\n");

        try {
            return (NotificationCompService) GenericServiceClientFactory.getService(nofitificationCompServiceLookupKey,
                    config.getNotificationCompUsername(),
                    keyStoreManager.decrypt(config.getNotificationCompPassword()),
                    NotificationCompService.class);

        } catch (Exception ex) {
            log.error("Could not initialize NotificationComp service: " + ex);
            throw new VaadinUIException("Could not initialize NotificationComp service.");
        }
    }

    public String sendNotification(NotificationInfo notificationInfo) {
        try {
            ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();

            try {
                log.info("Calling NotificationCompService on method sendNotification with notificationInfo : " +
                        writer.writeValueAsString(notificationInfo));
            } catch (IOException e) {
                log.warn("Could not map request NotificationInfo to string exception: " + e);
            }

            return getService().sendNotification(notificationInfo, getContextInfo(null));

        } catch (DoesNotExistException doesNotExistException) {
            log.error("Could not find user to send notification to: " + doesNotExistException.getMessage(), doesNotExistException);
            throw new VaadinUIException("notification.send.notification.not.found", true);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("UnforeseenException occurred when sending notification to: " + e.getMessage(), e);
            throw new VaadinUIException("notification.send.notification.fails", true);
        }
    }
}

