package nwu.student.sb.ui.views;

import ac.za.nwu.core.type.dto.TypeAttributeValueInfo;
import ac.za.nwu.core.type.dto.TypeInfo;
import ac.za.nwu.student.leader.dto.StudentLeaderSetupInfo;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import assemble.edu.exceptions.VersionMismatchException;
import com.google.common.eventbus.EventBus;
import com.vaadin.data.HasValue;
import com.vaadin.server.UserError;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import nwu.ac.za.framework.i18n.Translate;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.ui.SeverityType;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.sb.StudentBenefitsCrudUI;
import nwu.student.sb.events.ShowNotificationEvent;
import nwu.student.sb.ui.config.Config;
import nwu.student.sb.ui.model.dto.ExecutionContext;
import nwu.student.sb.ui.model.dto.TypeAttributeRequest;
import nwu.student.sb.ui.service.StudentLeaderProxyService;
import nwu.student.sb.ui.service.TypeProxyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

@SpringComponent
@UIScope
@Translate
@RefreshScope
public class StudentBenefitsAdminView extends VerticalLayout {

    private final Logger log = LoggerFactory.getLogger(StudentBenefitsAdminView.class.getName());

    @Autowired
    private Config config;
    @Autowired
    private VaadinI18n vaadinI18n;
    @Autowired
    private EventBus eventBus;
    @Autowired
    private TypeProxyService typeServiceProxy;
    @Autowired
    private StudentLeaderProxyService studentLeaderProxyService;

    private ExecutionContext executionContext;
    private ComboBox<TypeInfo> studentLeadershipPositionsComboBox;
    private ComboBox<String> yearComboBox;
    private Grid<StudentLeaderSetupInfo> studentLeaderBenefitGrid = new Grid<>();
    private Boolean gridLoaded = false;
    private BigInteger userFixedAmount;
    private Double userMaxPercentage;
    private BigDecimal userMaxAmount;
    private Integer userMonths;
    private BigDecimal userMonthlyAmount;
    private HorizontalLayout dropListAndYearHorizontalLayout;
    private List<StudentLeaderSetupInfo> studentLeaderSetupInfos;
    private Button copyYearDataButton;
    private String previousYear;

    public void initTheView(ExecutionContext executionContext) {
        if (!gridLoaded) {
            this.executionContext = executionContext;
            setSizeFull();
            setupUILayout();
            gridLoaded = true;
        }
    }

    private void setupUILayout() {
        setupUIComponents();
        setupEvents();
    }

    private void setupUIComponents() {
        // Get data for Droplist
        List<TypeInfo> leadershipPositions = getLeadershipPositions();

        // Create Droplist
        Label studentLeadershipPositionComboBoxLabel = new Label(vaadinI18n.getMessage("slb.student.leadership.positions.label"));
        studentLeadershipPositionsComboBox = new ComboBox<>();
        studentLeadershipPositionsComboBox.setItems(leadershipPositions);
        studentLeadershipPositionsComboBox.setItemCaptionGenerator(this::getTypeKeyTextValue);
        studentLeadershipPositionsComboBox.setWidth("250px");
        studentLeadershipPositionsComboBox.setEmptySelectionAllowed(false);
        studentLeadershipPositionsComboBox.setSelectedItem(leadershipPositions.get(0));

        List<String> years = new ArrayList<>();
        Year currentYear = Year.now();
        Year lastYear = currentYear.minusYears(1);
        Year nextYear = currentYear.plusYears(1);
        years.add(lastYear.toString());
        years.add(currentYear.toString());
        years.add(nextYear.toString());
        Label yearComboBoxLabel = new Label(vaadinI18n.getMessage("slb.year.label"));
        yearComboBox = new ComboBox<>();
        yearComboBox.setItems(years);
        yearComboBox.setSelectedItem(currentYear.toString());
        yearComboBox.setEmptySelectionAllowed(false);

        previousYear = lastYear.toString();
        copyYearDataButton = new Button(vaadinI18n.getMessage("slb.copy.year.data.label"));

        dropListAndYearHorizontalLayout = new HorizontalLayout();
        dropListAndYearHorizontalLayout.addComponents(studentLeadershipPositionComboBoxLabel, studentLeadershipPositionsComboBox, yearComboBoxLabel, yearComboBox, copyYearDataButton);
        dropListAndYearHorizontalLayout.setComponentAlignment(studentLeadershipPositionComboBoxLabel, Alignment.MIDDLE_CENTER);
        dropListAndYearHorizontalLayout.setComponentAlignment(yearComboBoxLabel, Alignment.MIDDLE_CENTER);

        this.addComponents(dropListAndYearHorizontalLayout, generateStudentLeaderAdminGrid(leadershipPositions.get(0).getTypeKey()));
        enableDisableCopyButton();
    }

    private String getTypeKeyTextValue(TypeInfo typeInfo) {
        return typeServiceProxy.getTypeLongDesc(typeInfo.getTypeKey(), executionContext.getSystemLanguageTypeKey());
    }

    private List<TypeInfo> getLeadershipPositions() {
        String VSS_CODE_HOEDANIG_SL = "vss.code.HOEDANIG.SL";
        List<TypeInfo> allRoles = typeServiceProxy.getTypesByCategory("vss.code.HOEDANIG", this.executionContext.getSystemLanguageTypeKey());
        List<TypeInfo> leadershipPositionRoles = new ArrayList<>();
        for (TypeInfo role : allRoles) {
            TypeAttributeRequest request = new TypeAttributeRequest(role.getTypeKey(), VSS_CODE_HOEDANIG_SL);
            List<TypeAttributeValueInfo> result = typeServiceProxy.getTypeAttributeValues(request);
            if (result != null) {
                if (result.get(0).getTypeAttributeValue().toUpperCase().contains("YES")) {
                    System.out.println(role.getTypeKey());
                    leadershipPositionRoles.add(role);
                }
            }
        }
        return leadershipPositionRoles;
    }

    private void setupEvents() {
        studentLeadershipPositionsComboBox.addValueChangeListener(e -> {
            ((ComboBox) e.getComponent()).setComponentError(null);
            StudentBenefitsCrudUI.getCurrent().clearErrors();
            generateStudentLeaderAdminGrid(e.getValue().getTypeKey());
            gridYearDataCheck();
            enableDisableCopyButton();
        });

        yearComboBox.addValueChangeListener((HasValue.ValueChangeListener<String>) e -> {
            ((ComboBox) e.getComponent()).setComponentError(null);
            StudentBenefitsCrudUI.getCurrent().clearErrors();
            previousYear = String.valueOf(Integer.valueOf(yearComboBox.getValue()) - 1);
            generateStudentLeaderAdminGrid(studentLeadershipPositionsComboBox.getSelectedItem().get().getTypeKey());
            gridYearDataCheck();
            enableDisableCopyButton();
        });

        copyYearDataButton.addClickListener((Button.ClickListener) e -> {
            ((Button) e.getComponent()).setComponentError(null);
            StudentBenefitsCrudUI.getCurrent().clearErrors();
            copyDataFromPreviousYear();
            generateStudentLeaderAdminGrid(studentLeadershipPositionsComboBox.getSelectedItem().get().getTypeKey());
            gridYearDataCheck();
        });
    }

    private void enableDisableCopyButton() {
        List<StudentLeaderSetupInfo> previousYearStudentLeaderSetupInfos
                = studentLeaderProxyService.getStudentLeaderSetups(studentLeadershipPositionsComboBox.getSelectedItem()
                .get().getTypeKey(), previousYear);

        if(!previousYearStudentLeaderSetupInfos.isEmpty() && studentLeaderSetupInfos.isEmpty()) {
            copyYearDataButton.setEnabled(true);
        } else {
            copyYearDataButton.setEnabled(false);
        }
    }

    private void copyDataFromPreviousYear() {
        List<StudentLeaderSetupInfo> previousYearStudentLeaderSetupInfos
                = studentLeaderProxyService.getStudentLeaderSetups(studentLeadershipPositionsComboBox.getSelectedItem()
                .get().getTypeKey(), previousYear);

        if (!previousYearStudentLeaderSetupInfos.isEmpty() && studentLeaderSetupInfos.isEmpty()) {
            for (StudentLeaderSetupInfo copyStudentLeadershipInfo : previousYearStudentLeaderSetupInfos) {
                copyStudentLeadershipInfo.setYear(Integer.valueOf(yearComboBox.getValue()));
                try {
                    studentLeaderProxyService.getService().createStudentLeaderSetup(copyStudentLeadershipInfo, new ContextInfo(config.getAppName()));
                } catch (DoesNotExistException | InvalidParameterException | MissingParameterException |
                        OperationFailedException | PermissionDeniedException | VersionMismatchException e) {
                    e.printStackTrace();
                    log.error(vaadinI18n.getMessage("slb.copy.year.data.failed") + ": " + e);
                    revertData(previousYearStudentLeaderSetupInfos);
                    throw new VaadinUIException(vaadinI18n.getMessage("slb.copy.year.data.failed"));
                }
            }
            eventBus.post(new ShowNotificationEvent(vaadinI18n.getMessage("slb.copy.year.data.success"), SeverityType.SUCCESS));
            yearComboBox.setComponentError(null);
            copyYearDataButton.setEnabled(false);
        } else {
            eventBus.post(new ShowNotificationEvent(vaadinI18n.getMessage("slb.copy.year.data.failed"), SeverityType.ERROR));
        }
    }

    private void revertData(List<StudentLeaderSetupInfo> previousYearStudentLeaderSetupInfos) {
        // Revert copied data
        for (StudentLeaderSetupInfo deleteCopyStudentLeadershipInfo : previousYearStudentLeaderSetupInfos) {
            deleteCopyStudentLeadershipInfo.setYear(Integer.valueOf(yearComboBox.getValue()));
            try {
                studentLeaderProxyService.getService().deleteStudentLeaderSetup(
                        deleteCopyStudentLeadershipInfo.getStudLeaderPositionTypeKey(),
                        yearComboBox.getValue(),
                        deleteCopyStudentLeadershipInfo.getLeadershipTypeKey(),
                        deleteCopyStudentLeadershipInfo.getFeeTypeKey(),
                        new ContextInfo(config.getAppName()));
            } catch (DoesNotExistException | InvalidParameterException | MissingParameterException
                    | OperationFailedException | PermissionDeniedException | VersionMismatchException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void gridYearDataCheck() {
        if (studentLeaderSetupInfos.isEmpty()) {
            log.error(vaadinI18n.getMessage("slb.error.admin.grid.nodata"));
            eventBus.post(new ShowNotificationEvent(vaadinI18n.getMessage("slb.error.admin.grid.nodata"), SeverityType.ERROR));
            yearComboBox.setComponentError(new UserError(vaadinI18n.getMessage("slb.error.admin.grid.nodata")));
        }
    }

    private Grid<StudentLeaderSetupInfo> generateStudentLeaderAdminGrid(String studentLeadershipPosition) {
        studentLeaderSetupInfos = studentLeaderProxyService.getStudentLeaderSetups(studentLeadershipPosition, yearComboBox.getValue());

        studentLeaderBenefitGrid.setSelectionMode(Grid.SelectionMode.NONE);
        studentLeaderBenefitGrid.removeAllColumns();
        studentLeaderBenefitGrid.setSizeFull();
        studentLeaderBenefitGrid.setHeightMode(HeightMode.ROW);
        studentLeaderBenefitGrid.setItems(studentLeaderSetupInfos);
        studentLeaderBenefitGrid.addColumn(this::getFeeTypeKeyDescription).setCaption(vaadinI18n.getMessage("slb.admin.grid.column.benefit")).setSortable(false);
        studentLeaderBenefitGrid.addColumn(StudentLeaderSetupInfo::getFixedAmount).setCaption(vaadinI18n.getMessage("slb.admin.grid.column.fixedAmount")).setSortable(false);
        studentLeaderBenefitGrid.addColumn(StudentLeaderSetupInfo::getMaxPercentage).setCaption(vaadinI18n.getMessage("slb.admin.grid.column.maxPercentage")).setSortable(false);
        studentLeaderBenefitGrid.addColumn(StudentLeaderSetupInfo::getMaxAmount).setCaption(vaadinI18n.getMessage("slb.admin.grid.column.maxAmount")).setSortable(false);
        studentLeaderBenefitGrid.addColumn(StudentLeaderSetupInfo::getNumberOfMonths).setCaption(vaadinI18n.getMessage("slb.admin.grid.column.months")).setSortable(false);
        studentLeaderBenefitGrid.addColumn(StudentLeaderSetupInfo::getMonthlyAmount).setCaption(vaadinI18n.getMessage("slb.admin.grid.column.monthlyAmount")).setSortable(false);
        studentLeaderBenefitGrid.addComponentColumn(this::editButton);

        return studentLeaderBenefitGrid;
    }

    private Button editButton(StudentLeaderSetupInfo studentLeaderSetupInfo) {
        Button editButton = new Button(vaadinI18n.getMessage("slb.admin.grid.edit.button"));
        editButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                StudentBenefitsCrudUI.getCurrent().clearErrors();
                userFixedAmount = studentLeaderSetupInfo.getFixedAmount();
                userMaxPercentage = studentLeaderSetupInfo.getMaxPercentage();
                userMaxAmount = studentLeaderSetupInfo.getMaxAmount();
                userMonths = studentLeaderSetupInfo.getNumberOfMonths();
                userMonthlyAmount = studentLeaderSetupInfo.getMonthlyAmount();
                showEditPopup(studentLeaderSetupInfo);
            }
        });
        return editButton;
    }

    private void showEditPopup(StudentLeaderSetupInfo studentLeaderSetupInfo) {
        Window window = new Window(vaadinI18n.getMessage("edit.popup.title"));
        window.setResizable(false);
        window.center();
        window.setWidth(350.0f, Unit.PIXELS);

        FormLayout userInputs = new FormLayout();
        userInputs.setCaptionAsHtml(true);
        userInputs.setCaption(vaadinI18n.getMessage("edit.popup.message") + " <strong>" + getFeeTypeKeyDescription(studentLeaderSetupInfo) + " </strong>");
        userInputs.addComponents(
                getFixedAmountTextField(studentLeaderSetupInfo),
                getMaxPercentageTextField(studentLeaderSetupInfo),
                getMaxAmountTextField(studentLeaderSetupInfo),
                getNumberOfMonthsTextField(studentLeaderSetupInfo),
                getMonthlyAmountTextField(studentLeaderSetupInfo)
        );
        Button updateButton = new Button(vaadinI18n.getMessage("edit.popup.button.continue"));
        updateButton.addStyleName("primary");

        VerticalLayout popupContent = new VerticalLayout();
        popupContent.setMargin(true);
        popupContent.addComponents(userInputs, updateButton);
        popupContent.setComponentAlignment(updateButton, Alignment.MIDDLE_RIGHT);
        window.setContent(popupContent);
        UI.getCurrent().addWindow(window);

        updateButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                StudentBenefitsCrudUI.getCurrent().clearErrors();
                if (save(studentLeaderSetupInfo)) {
                    generateStudentLeaderAdminGrid(studentLeadershipPositionsComboBox.getSelectedItem().get().getTypeKey());

                    window.close();
                }

            }
        });

        window.addCloseListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent closeEvent) {
                window.close();
            }
        });
    }

    private String getFeeTypeKeyDescription(StudentLeaderSetupInfo studentLeaderSetupInfo) {
        return typeServiceProxy.getTypeLongDesc(studentLeaderSetupInfo.getFeeTypeKey(), executionContext.getSystemLanguageTypeKey());
    }

    private TextField getFixedAmountTextField(StudentLeaderSetupInfo studentLeaderSetupInfo) {
        TextField textField = new TextField();
        textField.setValue(getColumnTextFieldValue(studentLeaderSetupInfo.getFixedAmount()));
        textField.setValueChangeMode(ValueChangeMode.BLUR);
        textField.setCaption(vaadinI18n.getMessage("slb.admin.grid.column.fixedAmount"));

        textField.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                StudentBenefitsCrudUI.getCurrent().clearErrors();
                String userInput = ((TextField) event.getComponent()).getValue();
                if (userInput.trim().length() > 0) {
                    try {
                        BigInteger fixedAmount = new BigInteger(userInput);
                        userFixedAmount = fixedAmount;
                    } catch (NumberFormatException e) {
                        log.error(vaadinI18n.getMessage("slb.error.admin.grid.input.fixedAmount") + ": " + e);
                        throw new VaadinUIException(vaadinI18n.getMessage("slb.error.admin.grid.input.fixedAmount"));
                    }
                } else {
                    userFixedAmount = null;
                }
            }
        });

        return textField;
    }

    private TextField getMaxPercentageTextField(StudentLeaderSetupInfo studentLeaderSetupInfo) {
        TextField textField = new TextField();
        textField.setValue(getColumnTextFieldValue(studentLeaderSetupInfo.getMaxPercentage()));
        textField.setValueChangeMode(ValueChangeMode.BLUR);
        textField.setCaption(vaadinI18n.getMessage("slb.admin.grid.column.maxPercentage"));

        textField.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                StudentBenefitsCrudUI.getCurrent().clearErrors();
                String userInput = ((TextField) event.getComponent()).getValue();
                if (userInput.trim().length() > 0) {
                    try {
                        Double maxPercentage = Double.valueOf(userInput);
                        if (maxPercentage == 0.00 || maxPercentage > 100.00) {
                            log.error(vaadinI18n.getMessage("slb.error.admin.grid.input.maxPercentage"));
                            throw new VaadinUIException(vaadinI18n.getMessage("slb.error.admin.grid.input.maxPercentage"));
                        } else {
                            userMaxPercentage = maxPercentage;
                        }
                    } catch (NumberFormatException e) {
                        log.error(vaadinI18n.getMessage("slb.error.admin.grid.input.maxPercentage") + ": " + e);
                        throw new VaadinUIException(vaadinI18n.getMessage("slb.error.admin.grid.input.maxPercentage"));
                    }
                } else {
                    userMaxPercentage = null;
                }
            }
        });

        return textField;
    }

    private TextField getMaxAmountTextField(StudentLeaderSetupInfo studentLeaderSetupInfo) {
        TextField textField = new TextField();
        textField.setValue(getColumnTextFieldValue(studentLeaderSetupInfo.getMaxAmount()));
        textField.setValueChangeMode(ValueChangeMode.BLUR);
        textField.setCaption(vaadinI18n.getMessage("slb.admin.grid.column.maxAmount"));

        textField.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                StudentBenefitsCrudUI.getCurrent().clearErrors();
                String userInput = ((TextField) event.getComponent()).getValue();
                if (userInput.trim().length() > 0) {
                    try {
                        BigDecimal maxAmount = new BigDecimal(userInput);
                        userMaxAmount = maxAmount;
                    } catch (NumberFormatException e) {
                        log.error(vaadinI18n.getMessage("slb.error.admin.grid.input.maxAmount") + ": " + e);
                        throw new VaadinUIException(vaadinI18n.getMessage("slb.error.admin.grid.input.maxAmount"));
                    }
                } else {
                    userMaxAmount = null;
                }
            }
        });

        return textField;
    }

    private TextField getNumberOfMonthsTextField(StudentLeaderSetupInfo studentLeaderSetupInfo) {
        TextField textField = new TextField();
        textField.setValue(getColumnTextFieldValue(studentLeaderSetupInfo.getNumberOfMonths()));
        textField.setValueChangeMode(ValueChangeMode.BLUR);
        textField.setCaption(vaadinI18n.getMessage("slb.admin.grid.column.months"));

        textField.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                StudentBenefitsCrudUI.getCurrent().clearErrors();
                String userInput = ((TextField) event.getComponent()).getValue();
                if (userInput.trim().length() > 0) {
                    try {
                    	Integer months = Integer.valueOf(userInput);
                        if (months == 0 || months > 12) {
                            log.error(vaadinI18n.getMessage("slb.error.admin.grid.input.months"));
                            throw new VaadinUIException(vaadinI18n.getMessage("slb.error.admin.grid.input.months"));
                        } else {
                            userMonths = months;
                        }
                    } catch (NumberFormatException e) {
                        log.error(vaadinI18n.getMessage("slb.error.admin.grid.input.months") + ": " + e);
                        throw new VaadinUIException(vaadinI18n.getMessage("slb.error.admin.grid.input.months"));
                    }
                } else {
                    userMonths = null;
                }
            }
        });

        return textField;
    }

    private TextField getMonthlyAmountTextField(StudentLeaderSetupInfo studentLeaderSetupInfo) {
        TextField textField = new TextField();
        textField.setValue(getColumnTextFieldValue(studentLeaderSetupInfo.getMonthlyAmount()));
        textField.setValueChangeMode(ValueChangeMode.BLUR);
        textField.setCaption(vaadinI18n.getMessage("slb.admin.grid.column.monthlyAmount"));

        textField.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> event) {
                StudentBenefitsCrudUI.getCurrent().clearErrors();
                String userInput = ((TextField) event.getComponent()).getValue();
                if (userInput.trim().length() > 0) {
                    try {
                    	BigDecimal monthlyAmount = new BigDecimal(userInput);
                        userMonthlyAmount = monthlyAmount;
                    } catch (NumberFormatException e) {
                        log.error(vaadinI18n.getMessage("slb.error.admin.grid.input.monthlyAmount") + ": " + e);
                        throw new VaadinUIException(vaadinI18n.getMessage("slb.error.admin.grid.input.monthlyAmount"));
                    }
                } else {
                    userMonthlyAmount = null;
                }
            }
        });

        return textField;
    }

    private String getColumnTextFieldValue(Object columnValue){
        String value = String.valueOf(columnValue);
        if(value.equals("null")) {
            return "";
        }
        return value;
    }

    private Boolean save(StudentLeaderSetupInfo studentLeaderSetupInfo) {
    	studentLeaderSetupInfo.setFixedAmount(userFixedAmount);
        studentLeaderSetupInfo.setMaxPercentage(userMaxPercentage);
        studentLeaderSetupInfo.setMaxAmount(userMaxAmount);
        studentLeaderSetupInfo.setNumberOfMonths(userMonths);
        studentLeaderSetupInfo.setMonthlyAmount(userMonthlyAmount);
        
        try {
            studentLeaderProxyService.getService().updateStudentLeaderSetup(studentLeaderSetupInfo, new ContextInfo(config.getAppName()));

            eventBus.post(new ShowNotificationEvent(vaadinI18n.getMessage("slb.success.save"), SeverityType.SUCCESS));
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException |
                OperationFailedException | PermissionDeniedException | VersionMismatchException e) {
            e.printStackTrace();
            log.error(vaadinI18n.getMessage("slb.error.save") + ": " + e);
            throw new VaadinUIException(vaadinI18n.getMessage("slb.error.save"));
        }

        return true;
    }
}
