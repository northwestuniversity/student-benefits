package nwu.student.sb.ui.model.dto;

import assemble.edu.common.dto.ContextInfo;

public class TypeAttributeRequest {
    private String typeKey = "";
    private String catagoryAttributeTypeKey = "";

    public TypeAttributeRequest(String typeKey, String catagoryAttributeTypeKey) {
        this.typeKey = typeKey;
        this.catagoryAttributeTypeKey = catagoryAttributeTypeKey;
    }

    public TypeAttributeRequest() {
    }

    public String getTypeKey() {
        return typeKey;
    }

    public void setTypeKey(String typeKey) {
        this.typeKey = typeKey;
    }

    public String getCatagoryAttributeTypeKey() {
        return catagoryAttributeTypeKey;
    }

    public void setCatagoryAttributeTypeKey(String catagoryAttributeTypeKey) {
        this.catagoryAttributeTypeKey = catagoryAttributeTypeKey;
    }

}
