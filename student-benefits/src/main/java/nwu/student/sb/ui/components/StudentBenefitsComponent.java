package nwu.student.sb.ui.components;

import ac.za.nwu.core.person.dto.PersonAffiliationInfo;
import ac.za.nwu.core.person.dto.PersonBiographicInfo;
import ac.za.nwu.core.type.dto.TypeAttributeValueInfo;
import ac.za.nwu.notification.dto.body.MessageBodyInfo;
import ac.za.nwu.notification.dto.body.TemplateInfo;
import ac.za.nwu.notification.dto.body.wrappers.ParameterNameWrapperInfo;
import ac.za.nwu.notification.dto.body.wrappers.ParameterValueWrapperInfo;
import ac.za.nwu.notification.dto.body.wrappers.SubjectHeaderInfo;
import ac.za.nwu.notification.dto.notifications.email.EmailInfo;
import ac.za.nwu.notification.dto.notifications.email.wrappers.LanguageAndTemplateParamWrapperInfo;
import ac.za.nwu.notification.dto.notifications.email.wrappers.RecipientWrapperInfo;
import ac.za.nwu.student.leader.dto.StudentLeaderSetupInfo;
import ac.za.nwu.student.leader.dto.StudentLeadershipInfo;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import com.google.common.eventbus.EventBus;
import com.vaadin.data.HasValue;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import nwu.ac.za.framework.i18n.Translate;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.ui.SeverityType;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.sb.StudentBenefitsCrudUI;
import nwu.student.sb.events.ShowNotificationEvent;
import nwu.student.sb.ui.config.ApplicationConfig;
import nwu.student.sb.ui.config.Config;
import nwu.student.sb.ui.designs.MainViewDesign;
import nwu.student.sb.ui.model.dto.ExecutionContext;
import nwu.student.sb.ui.model.dto.TypeAttributeRequest;
import nwu.student.sb.ui.service.NotificationCompProxyService;
import nwu.student.sb.ui.service.PersonProxyService;
import nwu.student.sb.ui.service.StudentLeaderProxyService;
import nwu.student.sb.ui.service.TypeProxyService;
import nwu.student.sb.ui.util.StudentBenefitsUIUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

@SpringComponent
@UIScope
@Translate
@RefreshScope
public class StudentBenefitsComponent extends MainViewDesign {

    private final Logger log = LoggerFactory.getLogger(StudentBenefitsComponent.class.getName());
    private static final DecimalFormat df = new DecimalFormat("0.00");

    @Autowired
    private PersonProxyService personProxyService;
    @Autowired
    private StudentLeaderProxyService studentLeaderProxyService;
    @Autowired
    private TypeProxyService typeServiceProxy;
    @Autowired
    private Config config;
    @Autowired
    private ApplicationConfig appConfig;
    @Autowired
    private VaadinI18n vaadinI18n;
    @Autowired
    private EventBus eventBus;

    @Autowired
    private NotificationCompProxyService notificationCompProxyService;

    @Autowired
    private ApplicationConfig applicationConfig;

    private PersonBiographicInfo personBiographic;
    private ExecutionContext executionContext;
    private HashMap<String, ComboBox> comboBoxHashmap = new HashMap<>();
    private HashMap<String, Label> selectionLabelHashmap = new HashMap<>();
    private HashMap<String, StudentLeaderSetupInfo> studentLeaderSetupInfoHashMap = new HashMap<>();
    private BigInteger posAmount;
    private String studentPosition;
    private Double amtToBePaidOut;
    private HashMap<String, StudentLeadershipInfo> studentInfo;
    private static final String VSS_CODE_HOEDANIG_SL = "vss.code.HOEDANIG.SL";
    private Boolean alreadySubmitted = false;

    public void initUI(ExecutionContext executionContext) {
        eventBus.register(this);
        this.executionContext = executionContext;
        fixDisclaimerFormatting();
        loadPersonDetails();
        studentPosition = loadPersonPosition();

        if (studentPosition == null) {
            determineStudentLeaderRollError();
        } else {
            populateStudentRole(studentPosition, appConfig.getACADEMIC_YEAR().toString());
        }

        ClickListener saveListener = new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                preSaveValidation();
                ArrayList<StudentLeadershipInfo> listToSave = mapUIDataToDTO();
                ArrayList<StudentLeadershipInfo> listToDelete = determineStudentLeadershipToDelete(listToSave);
                save(listToSave, listToDelete);
                if (sendEmail()) {
                    showConfirmationPopup();
                }
            }
        };

        getBtnSubmit().addClickListener(saveListener);

        getTxtUnivNum().addValueChangeListener((HasValue.ValueChangeListener<String>) event -> {
            eventBus.post(new ShowNotificationEvent(null, null));
            StudentBenefitsCrudUI.getCurrent().clearErrors();
            getTxtUnivNum().setComponentError(null);
            this.executionContext.setLookupUser(getTxtUnivNum().getValue());
            loadPersonDetails();
            studentPosition = loadPersonPosition();
            if (studentPosition == null) {
                clearComponents();
                determineStudentLeaderRollError();
            } else {
                populateStudentRole(studentPosition, appConfig.getACADEMIC_YEAR().toString());
            }
        });
    }

    private void determineStudentLeaderRollError() {
        // THis is for when it is an admin which types in a university number and that number is not a student leader.
        if (!this.executionContext.getAuthenticatedUser().equals(this.executionContext.getLookupUser()) || !this.executionContext.isAdmin()) {
            toggleComponentVisible(false);
            hideDisclaimer();
            throw new VaadinUIException("person.no.studentleadership.role", true);
        } else {
            toggleComponentVisible(false);
            hideDisclaimer();
        }
    }

    private void buildDisclaimerSection() {
        getDisclaimerLabel().setVisible(true);
        getDisclaimerHeadingLabel().setVisible(true);
        getDisclaimerCheckBox().setVisible(true);
        getDisclaimerCheckBox().setValue(false);

        getDisclaimerCheckBox().addValueChangeListener(new ValueChangeListener<Boolean>() {
            @Override
            public void valueChange(ValueChangeEvent<Boolean> event) {
                toggleComponentVisible(event.getValue());
            }
        });

    }

    private void toggleComponentVisible(Boolean visible) {
        studentRoleLbl.setVisible(visible);
        studentReceivableLayout.setVisible(visible);
        lblTotValueReceiveable.setVisible(visible);
        lblTotValueReceiveableAmount.setVisible(visible);
        studentBenefitsLayout.setVisible(visible);
        studentPayoutLayout.setVisible(visible);
        lblTotAmtPaidOut.setVisible(visible);
        lblTotAmtPaidOutCalculated.setVisible(visible);
        buttonVeritcalLayout.setVisible(visible);
        btnSubmit.setVisible(visible);
    }

    private ArrayList<StudentLeadershipInfo> determineStudentLeadershipToDelete(ArrayList<StudentLeadershipInfo> listToSave) {

        if (studentInfo != null) {
            HashMap<String, StudentLeadershipInfo> deleteMe = (HashMap<String, StudentLeadershipInfo>) studentInfo.clone();
            for (StudentLeadershipInfo item : listToSave) {
                deleteMe.remove(item.getFeeTypeKey());
            }

            ArrayList<StudentLeadershipInfo> deleteMeList = new ArrayList<StudentLeadershipInfo>();
            for (Entry<String, StudentLeadershipInfo> itemToDelete : deleteMe.entrySet()) {
                deleteMeList.add(itemToDelete.getValue());
                System.out.println("Delete me : " + itemToDelete.getValue().getFeeTypeKey());
            }
            return deleteMeList;

        } else {
            return null;
        }
    }

    private ArrayList<StudentLeadershipInfo> mapUIDataToDTO() {
        System.out.println("mapUIDataToDTO");

        ArrayList<StudentLeadershipInfo> saveList = new ArrayList<StudentLeadershipInfo>();
        for (Entry<String, ComboBox> entry : comboBoxHashmap.entrySet()) {
            ComboBox comobBox = entry.getValue();
            String feeType = entry.getKey();
            System.out.println(feeType);
            StudentLeadershipInfo studLeadershipInfo = new StudentLeadershipInfo();
            studLeadershipInfo.setUnivNumber(Integer.parseInt(this.executionContext.getLookupUser()));

            Optional selectedValue = comobBox.getSelectedItem();
            StudentLeaderSetupInfo setup = (StudentLeaderSetupInfo) comobBox.getData();
            if (selectedValue != null && selectedValue.isPresent()) {
                studLeadershipInfo.setAuditFunction("999");
                studLeadershipInfo.setCreateId(this.executionContext.getAuthenticatedUser());
                studLeadershipInfo.setFeeTypeKey(feeType);
                studLeadershipInfo.setYear(appConfig.getACADEMIC_YEAR());

                Integer selection = Math.round(new Double((String) selectedValue.get()).intValue());

                if (setup.getNumberOfMonths() != null && setup.getNumberOfMonths() > 0) {
                    Double monthlyAmount = setup.getMonthlyAmount().doubleValue();
                    Double amount = monthlyAmount * selection;
                    studLeadershipInfo.setNumberOfMonths(selection);
                    studLeadershipInfo.setCalculatedAmount(new BigDecimal(amount));
                }
                if (setup.getMaxPercentage() != null && setup.getMaxPercentage() > 0) {
                    Double posDbl = posAmount.doubleValue();
                    Double amount = posDbl * selection / 100;
                    studLeadershipInfo.setPercentage(new Double(selection));
                    studLeadershipInfo.setCalculatedAmount(new BigDecimal(amount));

                }
                if (studentInfo != null) {
                    StudentLeadershipInfo studentExistingInfo = studentInfo.get(feeType);
                    if (studentExistingInfo != null) {
                        studLeadershipInfo.setLegacyKeys(studentExistingInfo.getLegacyKeys());
                        studLeadershipInfo.setUpdateId(executionContext.getAuthenticatedUser());
                    }
                }
                saveList.add(studLeadershipInfo);
            }
        }
        return saveList;
    }

    private void preSaveValidation() {
        validateAmountToBePaidOut();
        validateBenefitsMaxPercentage();
    }

    private void validateBenefitsMaxPercentage() {

        Boolean validEntries = true;

        for (StudentLeadershipInfo item : mapUIDataToDTO()) {
            StudentLeaderSetupInfo studentLeaderSetupInfo = studentLeaderSetupInfoHashMap.get(item.getFeeTypeKey());
            if (studentLeaderSetupInfo != null) {    // Not all setup on studentLeaderSetupInfoHashMap
                if (item.getCalculatedAmount().compareTo(studentLeaderSetupInfo.getMaxAmount()) == 1) {
                    validEntries = false;
                    break;
                }
            }
        }

        if (!validEntries) {
            throw new VaadinUIException("max.percentage.reached", true);
        }
    }

    private void validateAmountToBePaidOut() {
        if (amtToBePaidOut == null || amtToBePaidOut < 0) {
            throw new VaadinUIException("amount.to.be.paid.out.less.than.0", true);
        } else if (amtToBePaidOut > appConfig.getMinAmountToBePaidOut()) {
            throw new VaadinUIException("amount.to.be.paid.out.less.then.min", true);
        }
    }

    private void save(ArrayList<StudentLeadershipInfo> listToSave, ArrayList<StudentLeadershipInfo> listToDelete) {
        for (StudentLeadershipInfo itemToSave : listToSave) {
            studentLeaderProxyService.maintainStudentLeadership(itemToSave);
        }

        if (listToDelete != null && listToDelete.size() > 0) {
            for (StudentLeadershipInfo itemToDelete : listToDelete) {
                String result = studentLeaderProxyService.deleteStudentLeadership(itemToDelete);
                System.out.println(result);
            }
        }
    }

    private String loadPersonPosition() {
        try {
            List<PersonAffiliationInfo> results = personProxyService.getPersonAffiliation(this.executionContext.getLookupUser());
            for (PersonAffiliationInfo item : results) {
                TypeAttributeRequest request = new TypeAttributeRequest(item.getAffiliationTypeKey(), VSS_CODE_HOEDANIG_SL);
                List<TypeAttributeValueInfo> result = typeServiceProxy.getTypeAttributeValues(request);
                if (result != null) {
                    if (result.get(0).getTypeAttributeValue().toUpperCase().contains("YES")) {
                        System.out.println(item.getAffiliationTypeKey());
                        return item.getAffiliationTypeKey();
                    }
                }
            }
            return null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException(e.getMessage(), e);
        }
    }

    private void populateStudentRole(String position, String year) {
        clearComponents();
        List<StudentLeaderSetupInfo> positionSetup = studentLeaderProxyService.getStudentLeaderSetups(position, year);
        // First time app initialization is showing labels from the designer .html file which should be hidden.
        if (!positionSetup.isEmpty()) {
            String posLongDesc = typeServiceProxy.getTypeLongDesc(position, this.executionContext.getSystemLanguageTypeKey());
            studentRoleLbl.setValue(posLongDesc);
        }

        if (positionSetup.isEmpty()) {
            log.error(vaadinI18n.getMessage("student.leadership.setup.missing"));
            throw new VaadinUIException(vaadinI18n.getMessage("student.leadership.setup.missing"));
        }

        posAmount = null;
        for (StudentLeaderSetupInfo setup : positionSetup) {

            if (setup.getFeeTypeKey().toUpperCase().contains("VSS.CODE.STUDLEADFEE.POSAMT")) {
                getlblTotValueReceiveableAmount().setValue(df.format(new Double(setup.getFixedAmount().toString())));
                posAmount = setup.getFixedAmount();
                amtToBePaidOut = posAmount.doubleValue();
                getLblTotAmtPaidOutCalculated().setCaption(df.format(amtToBePaidOut));
                break;
            }
        }

        studentInfo = studentLeaderProxyService.getStudentLeadershipInfo(executionContext.getLookupUser(), year);
        // Student can't update his selections once submitted for period
        if (studentInfo != null) {
            if (!executionContext.isAdmin()) {
                setScreenReadonly();
            } else { // Allow admin to view user selections
                getDisclaimerCheckBox().setValue(true);
                toggleComponentVisible(true);
            }
            hideDisclaimer();
        } else {
            buildDisclaimerSection();
            toggleComponentVisible(false);
        }

        for (StudentLeaderSetupInfo setup : positionSetup) {
            if (setup.getFeeTypeKey().toUpperCase().contains("VSS.CODE.STUDLEADFEE.POSAMT")) {
                // Do nothing
            } else {
                // Formatting
                if (setup.getMaxAmount() != null) {
                    setup.setMaxAmount(setup.getMaxAmount().setScale(2));
                }
                if (setup.getMonthlyAmount() != null) {
                    setup.setMonthlyAmount(setup.getMonthlyAmount().setScale(2));
                }

                addBenefitRow(setup, posAmount, studentInfo);
            }
        }

        if (alreadySubmitted) {
            eventBus.post(new ShowNotificationEvent(vaadinI18n.getMessage("validation.main.already.completed"),
                    SeverityType.WARNING));
        }
    }

    private void setScreenReadonly() {
        getStudentBenefitsLayout().setEnabled(false);
        getBtnSubmit().setEnabled(false);
    }

    private void addBenefitRow(StudentLeaderSetupInfo setup, BigInteger posAmount, HashMap<String, StudentLeadershipInfo> studentInfo) {
        MarginInfo marginInfo = new MarginInfo(false, false, false, false);
        VerticalLayout benefitVerticalLayout = new VerticalLayout();
        benefitVerticalLayout.setMargin(marginInfo);

        setFeeTypeCaption(setup, benefitVerticalLayout, posAmount);
        HorizontalLayout benefitRowHLayout = new HorizontalLayout();

        benefitRowHLayout.setMargin(marginInfo);
        Label lblTotalAmount = new Label("");
        selectionLabelHashmap.put(setup.getFeeTypeKey(), lblTotalAmount);
        addDropDown(benefitRowHLayout, setup, studentInfo);

        benefitRowHLayout.addComponent(lblTotalAmount);
        benefitVerticalLayout.addComponent(benefitRowHLayout);
        getStudentBenefitsLayout().addComponent(benefitVerticalLayout);
    }

    private void setFeeTypeCaption(StudentLeaderSetupInfo setup, VerticalLayout benefitVerticalLayout, BigInteger posAmount) {
        String caption = "";
        String feeTypeLongDesc = typeServiceProxy.getTypeLongDesc(setup.getFeeTypeKey(), this.executionContext.getSystemLanguageTypeKey());

        List<String> params = new ArrayList<>();
        if (setup.getNumberOfMonths() != null && setup.getNumberOfMonths() > 0) {
            params.add(setup.getMonthlyAmount().toString());
            caption = feeTypeLongDesc + " " + vaadinI18n.getMessage("amount.per.month.cost", params);
        }
        if (setup.getMaxPercentage() != null && setup.getMaxPercentage() > 0) {
            params.add(df.format(new Double(posAmount.longValue())));
            caption = feeTypeLongDesc + " " + vaadinI18n.getMessage("percentage.of.rand.cost", params);
        }
        benefitVerticalLayout.setCaption(caption);
    }

    private ComboBox<String> addDropDown(HorizontalLayout benefitRowVerticalLayout, StudentLeaderSetupInfo setup, HashMap<String, StudentLeadershipInfo> studentInfo) {
        ComboBox<String> benefitComboBox = new ComboBox<>();
        benefitComboBox.setWidth("100px");
        benefitComboBox.setData(setup);

        List<String> options = new ArrayList<>();
        boolean isMonths = false;
        boolean isPercentage = false;
        if (setup.getNumberOfMonths() != null && setup.getNumberOfMonths() > 0) {
            isMonths = true;
            for (int i = 0; i < setup.getNumberOfMonths(); i++) {
                options.add(Integer.toString(i + 1));
            }
        }
        if (setup.getMaxPercentage() != null && setup.getMaxPercentage() > 0) {
            isPercentage = true;
            double numberOfIterations = setup.getMaxPercentage() / 10;
            for (int i = 0; i < numberOfIterations; i++) {
                options.add(Integer.toString((i + 1) * 10));
            }
        }

        benefitComboBox.setItems(options);
        benefitComboBox.setRequiredIndicatorVisible(false);
        benefitComboBox.setEmptySelectionAllowed(true);

        // Set student selection
        if (studentInfo != null) {
            StudentLeadershipInfo studentSelection = studentInfo.get(setup.getFeeTypeKey());

            if (studentSelection != null) {
                String selection = null;
                if (isMonths) {
                    selection = studentSelection.getNumberOfMonths().toString();
                    alreadySubmitted = true;
                }
                if (isPercentage) {
                    int intValue = studentSelection.getPercentage().intValue();
                    selection = Integer.toString(intValue);
                    alreadySubmitted = true;
                }
                benefitComboBox.setValue(selection);
                // Simulate event change on combobox, to calculate and setup labels.
                selectionChangedEvent(setup, Integer.parseInt(selection));
            }
        }

        benefitRowVerticalLayout.addComponent(benefitComboBox);
        benefitComboBox.addValueChangeListener(new ValueChangeListener<String>() {

            @Override
            public void valueChange(ValueChangeEvent<String> event) {
                StudentLeaderSetupInfo setupData = (StudentLeaderSetupInfo) benefitComboBox.getData();
                if (event.getValue() != null) {
                    Integer selection = Integer.parseInt(event.getValue());
                    System.out.println("event on " + setupData.getFeeTypeKey() + " : " + selection);
                    selectionChangedEvent(setup, selection);

                } else {
                    selectionChangedEvent(setup, 0);
                }
            }
        });

        comboBoxHashmap.put(setup.getFeeTypeKey(), benefitComboBox);
        return benefitComboBox;
    }

    private void selectionChangedEvent(StudentLeaderSetupInfo setup, Integer selection) {
        Label lblTotalAmount = selectionLabelHashmap.get(setup.getFeeTypeKey());

        if (setup.getNumberOfMonths() != null && setup.getNumberOfMonths() > 0) {
            BigDecimal amount = setup.getMonthlyAmount().multiply(new BigDecimal(selection));
            amount = amount.setScale(2);
            lblTotalAmount.setCaption(amount.toString());
        }

        if (setup.getMaxPercentage() != null && setup.getMaxPercentage() > 0) {
            Integer posDbl = posAmount.intValue();
            Integer amount = posDbl * selection / 100;
            lblTotalAmount.setCaption(df.format(amount));
        }

        if (setup.getMaxAmount() != null) {
            studentLeaderSetupInfoHashMap.put(setup.getFeeTypeKey(), setup);
        }

        calculateTotalToBePaidOut();
    }

    private void calculateTotalToBePaidOut() {
        Double totalBenefitsSelectedAmt = new Double(0);
        for (Entry<String, Label> entry : selectionLabelHashmap.entrySet()) {
            Label calcAmountLabel = entry.getValue();
            String calculatedAmt = calcAmountLabel.getCaption();
            System.out.println(entry.getKey() + "/" + calculatedAmt);
            if (calcAmountLabel != null && calculatedAmt != null) {
                double calcAmt = Double.parseDouble(calculatedAmt);
                if (calcAmt > 0) {
                    totalBenefitsSelectedAmt = totalBenefitsSelectedAmt + calcAmt;
                }
            }
        }

        Double positionAmt = new Double(posAmount.doubleValue());
        if (totalBenefitsSelectedAmt != null && totalBenefitsSelectedAmt > 0) {
            amtToBePaidOut = positionAmt - totalBenefitsSelectedAmt;
        } else {
            amtToBePaidOut = positionAmt;
        }
        getLblTotAmtPaidOutCalculated().setCaption(df.format(amtToBePaidOut));
        StudentBenefitsCrudUI.getCurrent().clearErrors();
    }

    private void loadPersonDetails() {
        try {
            personBiographic = personProxyService.getPersonBiographicByLang(executionContext.getLookupUser(), "vss.code.Language.3");
            String title = StudentBenefitsUIUtils.convertVssTypeKeyToName(personBiographic.getTitleTypeKey());
            this.txtUnivNum.setValue(personBiographic.getUnivNumber());
            this.lblTitleInitialsSurname.setValue(title + " " + personBiographic.getInitials() + " " + personBiographic.getLastName());
            if (!executionContext.isAdmin()) {
                txtUnivNum.setReadOnly(true);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException(e.getMessage(), e);
        }
    }

    private void clearComponents() {
        alreadySubmitted = false;
        getlblTotValueReceiveableAmount().setValue("");
        getStudentBenefitsLayout().removeAllComponents();

        comboBoxHashmap = new HashMap<>();
        selectionLabelHashmap = new HashMap<>();
        studentLeaderSetupInfoHashMap = new HashMap<>();
        studentInfo = null;
        amtToBePaidOut = null;
        getLblTotAmtPaidOutCalculated().setCaption("");
    }

    @Translate("slb.universityNumberLabel.caption")
    public Label getUniversityNumberLable() {
        return lblUnivNum;
    }

    @Translate("slb.btnSubmit.caption")
    public Button getBtnSubmit() {
        return btnSubmit;
    }

    public HorizontalLayout getStudentReceivableLayout() {
        return studentReceivableLayout;
    }

    @Translate("slb.tot.value.receivable")
    public Label getlblTotValueReceiveable() {
        return lblTotValueReceiveable;
    }

    // Will be set in code
    public Label getlblTotValueReceiveableAmount() {
        return lblTotValueReceiveableAmount;
    }

    public VerticalLayout getStudentBenefitsLayout() {
        return studentBenefitsLayout;
    }

    public HorizontalLayout getStudentPayoutLayout() {
        return studentPayoutLayout;
    }

    public Label getLblTotAmtPaidOutCalculated() {
        return lblTotAmtPaidOutCalculated;
    }

    @Translate("slb.tot.amount.tobe.paidout")
    public Label getLblTotAmtPaidOut() {
        return lblTotAmtPaidOut;
    }

    public TextField getTxtUnivNum() {
        return txtUnivNum;
    }

    public VerticalLayout getDisclaimerLayout() {
        return disclaimerLayout;
    }

    @Translate("disclaimer.heading")
    public Label getDisclaimerHeadingLabel() {
        return disclaimerHeadingLabel;
    }

    @Translate("disclaimer.details")
    public Label getDisclaimerLabel() {
        return disclaimerLabel;
    }

    @Translate("terms.and.conditions.accept")
    public CheckBox getDisclaimerCheckBox() {
        return disclaimerCheckBox;
    }

    private void fixDisclaimerFormatting() {
        getDisclaimerHeadingLabel().addStyleName("v-label-wrap");
        getDisclaimerHeadingLabel().setContentMode(ContentMode.HTML);
        getDisclaimerLabel().addStyleName("v-label-wrap");
        getDisclaimerLabel().setContentMode(ContentMode.HTML);
    }

    private void hideDisclaimer() {
        getDisclaimerLabel().setVisible(false);
        getDisclaimerHeadingLabel().setVisible(false);
        getDisclaimerCheckBox().setVisible(false);
    }

    @Translate
    private Boolean sendEmail() {
        EmailInfo emailInfo = new EmailInfo();
        emailInfo.setFromAddress(vaadinI18n.getMessage("email.from.address"));

        HashMap<RecipientWrapperInfo, LanguageAndTemplateParamWrapperInfo> recipientsLanguageTemplateParam = new HashMap<>();
        RecipientWrapperInfo recipientWrapperInfo = new RecipientWrapperInfo();
        recipientWrapperInfo.setRecipient(personBiographic.getUnivNumber());
        LanguageAndTemplateParamWrapperInfo languageAndTemplateParamWrapperInfo = new LanguageAndTemplateParamWrapperInfo();
        languageAndTemplateParamWrapperInfo.setMessageCatTemplateParameters(getEmailTemplateReplacementValues());
        recipientsLanguageTemplateParam.put(recipientWrapperInfo, languageAndTemplateParamWrapperInfo);
        emailInfo.setToRecipientAndParameters(recipientsLanguageTemplateParam);

        TemplateInfo subjectTemplateInfo = new TemplateInfo();
        subjectTemplateInfo.setApplication(config.getAppName().toUpperCase());
        subjectTemplateInfo.setMessageCatalogKey("email.save.successful.subject");
        subjectTemplateInfo.setVersion("GLOBAL");
        SubjectHeaderInfo subjectHeaderInfo = new SubjectHeaderInfo();
        subjectHeaderInfo.setTemplateInfo(subjectTemplateInfo);
        emailInfo.setSubjectHeaderInfo(subjectHeaderInfo);

        TemplateInfo messageTemplateInfo = new TemplateInfo();
        messageTemplateInfo.setApplication(config.getAppName().toUpperCase());
        messageTemplateInfo.setMessageCatalogKey("email.save.successful");
        messageTemplateInfo.setVersion("GLOBAL");
        MessageBodyInfo messageBodyInfo = new MessageBodyInfo();
        messageBodyInfo.setTemplateInfo(messageTemplateInfo);
        emailInfo.setMessageBodyInfo(messageBodyInfo);

        try {
            String emailSent = notificationCompProxyService.getService().sendNotificationEMail(emailInfo, new ContextInfo(config.getAppName()));
            if (emailSent.toLowerCase().contains("success")) {
                eventBus.post(new ShowNotificationEvent(vaadinI18n.getMessage("user.feedback.email.send.successful"), SeverityType.SUCCESS));
            }
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            e.printStackTrace();
            throw new VaadinUIException(vaadinI18n.getMessage("user.feedback.email.send.unsuccessful") + " " + e.getMessage(), e.getCause());
        }

        return true;
    }

    private HashMap<ParameterNameWrapperInfo, ParameterValueWrapperInfo> getEmailTemplateReplacementValues() {
        HashMap<ParameterNameWrapperInfo, ParameterValueWrapperInfo> emailParameters = new HashMap<>();
        ParameterNameWrapperInfo parameterNameWrapperInfo;
        ParameterValueWrapperInfo parameterValueWrapperInfo;

        parameterNameWrapperInfo = new ParameterNameWrapperInfo();
        parameterNameWrapperInfo.setParameterName("Year");
        parameterValueWrapperInfo = new ParameterValueWrapperInfo();
        parameterValueWrapperInfo.setParameterValue(Year.now().toString());
        emailParameters.put(parameterNameWrapperInfo, parameterValueWrapperInfo);

        return emailParameters;
    }

    @Translate
    private void showConfirmationPopup() {
        Label popupMessage = new Label(vaadinI18n.getMessage("ui.popup.message"));
        popupMessage.setContentMode(ContentMode.HTML);
        popupMessage.setSizeFull();
        Button button = new Button(vaadinI18n.getMessage("ui.popup.button.ok"));
        Window window = new Window(vaadinI18n.getMessage("ui.popup.title"));
        window.setResizable(false);
        window.center();
        window.setWidth(300.0f, Unit.PIXELS);
        VerticalLayout popupContent = new VerticalLayout();
        popupContent.setMargin(true);
        popupContent.addComponents(popupMessage,button);
        popupContent.setComponentAlignment(button, Alignment.MIDDLE_RIGHT);
        window.setContent(popupContent);
        UI.getCurrent().addWindow(window);

        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                window.close();
                navigateToDIY();
            }
        });

        window.addCloseListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent closeEvent) {
                window.close();
                navigateToDIY();
            }
        });
    }

    private void navigateToDIY(){
        if (UI.getCurrent().getLocale().getLanguage().equals("af")) {
            getUI().getPage().setLocation(applicationConfig.getDIY_UrlAF());
        } else {
            getUI().getPage().setLocation(applicationConfig.getDIY_UrlEN());
        }
    }
}
