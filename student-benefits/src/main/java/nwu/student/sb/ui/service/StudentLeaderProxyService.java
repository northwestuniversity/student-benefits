package nwu.student.sb.ui.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import ac.za.nwu.registry.utility.GenericServiceClientFactory;
import ac.za.nwu.student.leader.dto.StudentLeaderSetupInfo;
import ac.za.nwu.student.leader.dto.StudentLeadershipInfo;
import ac.za.nwu.student.leader.service.StudentLeaderService;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import assemble.edu.exceptions.VersionMismatchException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.sb.ui.config.Config;

@Service
@RefreshScope
public class StudentLeaderProxyService extends AbstractServiceProxy<StudentLeaderService> {

	@Autowired
	private Config config;

	@Autowired
	private KeyStoreManager keyStoreManager;
	
    @Autowired
    private VaadinI18n vaadinI18n;

	@Override
	protected StudentLeaderService initService() throws Exception {
		String wsMajorVersion = ServiceRegistryLookupUtility
				.getMajorVersion(config.getConfigProperty(Config.STUDENT_API_VERSION));

		String studentServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
				StudentLeaderService.STUDENTLEADERSERVICE, wsMajorVersion, config.getWebServiceDatabase(),
				config.getRuntimeEnvironment());

		// TODO NINA
//        String decryptedPassword = keyStoreManager.decrypt(config.getStudentServiceReadPassword());
		try {
			return (StudentLeaderService) GenericServiceClientFactory.getService(studentServiceLookupKey,
					config.getConfigProperty(Config.WS_SAPI_USER_NAME),
					config.getConfigProperty(Config.WS_SAPI_USER_NAME_PASSWORD), StudentLeaderService.class);

		} catch (Exception ex) {
			log.error("Could not initialise student leadership service error: " + ex.getMessage(), ex);
			throw new VaadinUIException("error.service.student.initialise", true, ex);
		}
	}

	public HashMap<String, StudentLeadershipInfo> getStudentLeadershipInfo(String univNumber, String year) {
		List<StudentLeadershipInfo> info = null;
		try {
			HashMap<String, StudentLeadershipInfo> studentInfo = new HashMap<String, StudentLeadershipInfo>();
			info = getService().getStudentLeaderships(univNumber, year, new ContextInfo(config.getAppName()));
			for (StudentLeadershipInfo item : info) {
				studentInfo.put(item.getFeeTypeKey(), item);
			}
			return studentInfo;
		} catch (DoesNotExistException de) {
			// Ignore as not all will have data
		} catch (PermissionDeniedException pe) {
			pe.printStackTrace();
			throw new VaadinUIException("student.leadership.permission.missing");
		} catch (InvalidParameterException | MissingParameterException | OperationFailedException
				| VersionMismatchException e) {
			throw new VaadinUIException("student.leadership.sometihng.wrong");
		}
		return null;
		
		
	}
	
	public List<StudentLeaderSetupInfo> getStudentLeaderSetups(String positionTypeKey, String year) {
		List<StudentLeaderSetupInfo> results;
		try {
			results = getService().getStudentLeaderSetups(positionTypeKey, year, new ContextInfo(config.getAppName()));
		} catch (DoesNotExistException de) {
			results = new ArrayList<>();
			de.printStackTrace();
		} catch (PermissionDeniedException pe) {
			pe.printStackTrace();
			throw new VaadinUIException("student.leadership.permission.missing");
		} catch (InvalidParameterException | MissingParameterException | OperationFailedException
				| VersionMismatchException e) {
			throw new VaadinUIException("student.leadership.sometihng.wrong");
		}
		return results;
	}

	public StudentLeadershipInfo maintainStudentLeadership(StudentLeadershipInfo studLeadership) {
		HashMap<String, String> keys = studLeadership.getLegacyKeys();
		String existingKey = null;
		if (keys != null) {
			existingKey = studLeadership.getLegacyKeys()
					.get(StudentLeadershipInfo.LEGACY_KEY_STUDENTLEADERSHIP_KSTUDENTLEADERSHIPID);
			
		}
		if (existingKey == null) {
			try {
				studLeadership = getService().createStudentLeadership(studLeadership,
						new ContextInfo(config.getAppName()));
			} catch (DoesNotExistException | InvalidParameterException | MissingParameterException
					| OperationFailedException | PermissionDeniedException | VersionMismatchException e) {
				String errMsg = vaadinI18n.getMessage("save.unsuccessful");
	            throw new VaadinUIException(errMsg + " " + e.getMessage());

			}
		} else {
			try {
				studLeadership = getService().updateStudentLeadership(studLeadership,
						new ContextInfo(config.getAppName()));
			} catch (DoesNotExistException | InvalidParameterException | MissingParameterException
					| OperationFailedException | PermissionDeniedException | VersionMismatchException e) {
				String errMsg = vaadinI18n.getMessage("save.unsuccessful");
	            throw new VaadinUIException(errMsg + " " + e.getMessage());
			}
		}
		return studLeadership;
	}
	
	public String deleteStudentLeadership(StudentLeadershipInfo studLeadership) {
		HashMap<String, String> keys = studLeadership.getLegacyKeys();
		String existingKey = null;
		if (keys != null) {
			existingKey = studLeadership.getLegacyKeys()
					.get(StudentLeadershipInfo.LEGACY_KEY_STUDENTLEADERSHIP_KSTUDENTLEADERSHIPID);
			
		}

			try {
				int univNumber = studLeadership.getUnivNumber();
				 String result = getService().deleteStudentLeadership(Integer.toString(univNumber), studLeadership.getYear().toString(), studLeadership.getFeeTypeKey(), new ContextInfo(config.getAppName()));
				 return result;
			} catch (DoesNotExistException | InvalidParameterException | MissingParameterException
					| OperationFailedException | PermissionDeniedException | VersionMismatchException e) {
				String errMsg = vaadinI18n.getMessage("save.unsuccessful");
	            throw new VaadinUIException(errMsg + " " + e.getMessage());
			}
			
	}

}
