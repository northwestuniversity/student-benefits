package nwu.student.sb.ui.model.dto;

public class ExecutionContext {

    private String authenticatedUser;
    private Boolean isUserAuthenticated;
    private String lookupUser;
    private String systemLanguageTypeKey;
    private boolean isAdmin;
    
    public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getAuthenticatedUser() {
        return authenticatedUser;
    }

    public void setAuthenticatedUser(String authenticatedUser) {
        this.authenticatedUser = authenticatedUser;
    }

    public String getLookupUser() {
        return lookupUser;
    }

    public void setLookupUser(String lookupUser) {
        this.lookupUser = lookupUser;
    }

    public Boolean isUserAuthenticated() {
        return isUserAuthenticated;
    }

    public void setUserAuthenticated(Boolean isUserAuthenticated) {
        this.isUserAuthenticated = isUserAuthenticated;
    }

    public void setSystemLanguageTypeKey(String sysLangTypeKey) {
        this.systemLanguageTypeKey= sysLangTypeKey;
    }

    public String getSystemLanguageTypeKey() {
        return systemLanguageTypeKey;
    }

}
