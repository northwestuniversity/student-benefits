package nwu.student.sb.ui.util;

public class StudentBenefitsUIUtils {

    public static String convertVssTypeKeyToName(String vssTypeKey) {
        String[] output = vssTypeKey.split("\\.");

        if(output.length > 1) {
            return output[4];
        } else {
            return output[0];
        }
    }

    public static String splitOnVssKey(String value, int length) {
        String[] split = value.split("\\.");
        return split[length];
    }
}
