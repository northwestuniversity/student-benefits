package nwu.student.sb.ui.service;

import java.util.List;

import ac.za.nwu.core.person.dto.EntityExternalReferenceInfo;
import nwu.ac.za.framework.auth.KeyStoreManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ac.za.nwu.core.person.dto.PersonAffiliationInfo;
import ac.za.nwu.core.person.dto.PersonBiographicInfo;
import ac.za.nwu.core.person.service.PersonService;
import ac.za.nwu.core.person.service.factory.PersonServiceClientFactory;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.sb.ui.config.Config;

@Service
public class PersonProxyService extends AbstractServiceProxy<PersonService> {

    @Autowired
    private Config config;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Override
    protected PersonService initService() throws Exception {

        String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getIdentityApiVersion());
        String appRuntimeEnv = config.getRuntimeEnvironment();
        String personServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(PersonServiceClientFactory.PERSONSERVICE, wsMajorVersion, null, appRuntimeEnv);
        System.out.println(personServiceLookupKey);
        try {
            String decryptedPassword = keyStoreManager.decrypt(config.getIApisReadPassword());

            
            return PersonServiceClientFactory.getPersonService(
                    personServiceLookupKey, config.getIApisReadUsername(), decryptedPassword);
        } catch (Exception ex) {
            log.error("Could not initialize Person service: " + ex);
            throw new VaadinUIException("Could not initialize Person service.");
        }
    }

    public PersonBiographicInfo getPersonBiographicByLang(String lookupUser, String systemLanguage) {
        try {
            log.info("Calling PersonService on method getPersonBiographicByLang: \n" +
                    "lookupUser: " + lookupUser + "\n" +
                    "systemLanguage: " + systemLanguage);

            return getService().getPersonBiographicByLang(lookupUser, systemLanguage, new ContextInfo(config.getAppName()));
        } catch (DoesNotExistException | InvalidParameterException | OperationFailedException | MissingParameterException | PermissionDeniedException e) {
            log.info("Could not retrieve person from person service: " + e);

            throw new RuntimeException(e);
        }
    }
    public PersonBiographicInfo getPersonBiographic(String lookupUser, String systemLanguageTypeKey) {
        try {
            return getService().getPersonBiographicByLang(lookupUser, systemLanguageTypeKey, new ContextInfo(config.getAppName()));
        } catch (InvalidParameterException | OperationFailedException | MissingParameterException | PermissionDeniedException e) {
            log.error("Could not retrieve person from person service: " + e);

            throw new VaadinUIException("Could not get person biographic for user: " + lookupUser + ": " + e);
        } catch (DoesNotExistException e1) {
            log.error("error.person.doesnot.exist " + lookupUser);
            throw new VaadinUIException("error.person.doesnot.exist", true);
        } catch (Exception ex) {
            log.error("Could not get Person Biographic for user: " + lookupUser + " : " + ex);
            throw new VaadinUIException("Could not getPersonBiographic for user: " + lookupUser);
        }
    }

    public List<PersonAffiliationInfo> getPersonAffiliation(String lookupUser) {
        try {
        	List<PersonAffiliationInfo> results = getService().getPersonAffiliation(lookupUser, new ContextInfo(config.getAppName()));        	
        	return results;
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException | PermissionDeniedException | OperationFailedException e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException(e.getMessage());
        } catch (Exception ex) {
            log.error("Could not get Person Affiliation for user: " + lookupUser + " : " + ex);
            throw new VaadinUIException("Could not get Person Affiliation for user: " + lookupUser);
        }
    }

    public List<EntityExternalReferenceInfo> getEntityExternalReferenceByTypeKey(String externalReferenceTypeKey, String externalReferenceValue) {
        try {
            return getService().getEntityExternalReferenceByTypeKey(externalReferenceTypeKey, externalReferenceValue.toUpperCase(),  new ContextInfo(config.getAppName()));
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException | PermissionDeniedException | OperationFailedException e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException("Could not validate accreditation number");
        } catch (Exception ex) {
            log.error("Could not validate accreditation number");
            throw new VaadinUIException("Could not validate accreditation number: "+externalReferenceValue);
        }
    }
}
