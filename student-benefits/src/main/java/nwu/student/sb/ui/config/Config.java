package nwu.student.sb.ui.config;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.PropertyResolver;
import org.springframework.stereotype.Component;

import assemble.edu.common.dto.ContextInfo;

@Component
public class Config {
	
    private final String WS_IAPI_READ_USER_NAME = "ws_iapiapp_read_username";
    private final String WS_IAPI_READ_USER_NAME_PASSWORD = "ws_iapiapp_read_username_password";
    private final String ENVIRONMENT = "runtimeEnvironment";
    private final String CAS_ENABLED = "cas.enabled";
    private final String SERVICE_LOOKUP_DATABASE = "service.lookup.database";
    private final String CONFIGURATION_MANAGEMENT_API_VERSION = "configuration-management.api.version";
    private final String CONFIGURATION_MANAGEMENT_API_VERSION_NAME = "configuration-management.api.version.name";
    private final String IDENTITY_API_VERSION = "identity.api.version";
    private final String APPLICATION_NAME = "application.name";
    private final String APPLICATION_VERSION = "application.version";
    
    
    private static final String SUPPORTING_SERVICE_CRUD_USERNAME = "supporting.service.CRUD.read.username";
    private static final String SUPPORTING_SERVICE_CRUD_PASSWORD = "supporting.service.CRUD.password";
    private static final String SUPPORTING_SERVICE_CRUD_DATABASE = "supporting.service.CRUD.database";

	private static final String WS_SUPPORTINGSERVICES_READ_USER_NAME_PASSWORD = "framework.service.supportingservices.password";
	private static final String WS_SUPPORTINGSERVICES_READ_USER_NAME = "framework.service.supportingservices.username";

    private static final String DOCUMENT_MANAGEMENT_API_VERSION = "document.management.api.version";

    private static final String DOCUMENT_SERVICE_USERNAME = "document.service.username";
    private static final String DOCUMENT_SERVICE_PASSWORD = "document.service.password";

    private static final String DOCUMENT_SERVICE_CRUD_USERNAME = "document.service.crud.username";
    private static final String DOCUMENT_SERVICE_CRUD_PASSWORD = "document.service.crud.password";


    private static final String SUPPORTING_SERVICES_VERSION = "supporting.service.version";

    private final String WEBSERVICE_DATABASE = "webservice.database";
    
    public final static String WS_SAPI_USER_NAME = "ws_sapiapp_username";
    public final static String WS_SAPI_USER_NAME_PASSWORD = "ws_sapiapp_username_password";
    public final static String STUDENT_API_VERSION = "student.api.version";

    private final String NOTIFICATION_API_VERSION = "notification.api.version";
    private final String NOTIFICATION_COMP_USERNAME = "notification.comp.read.username";
    private final String NOTIFICATION_COMP_PASSWORD = "notification.comp.read.password";

    private final ContextInfo contextInfo = new ContextInfo();

    @Autowired
    private PropertyResolver propertyResolver;

    private String supportingServicesVersion;

    private String documentManagementAPIVersion;

    private String documentServiceUsername;
    private String documentServicePassword;

    private String documentServiceCrudUsername;
    private String documentServiceCrudPassword;

    private String notificationAPIVersion;
    private String notificationCompUsername;
    private String notificationCompPassword;

    public String getSupportingServiceCrudUsername() {
        return propertyResolver.getProperty(SUPPORTING_SERVICE_CRUD_USERNAME);
    }

    public String getSupportingServiceCrudPassword(){
        return propertyResolver.getProperty(SUPPORTING_SERVICE_CRUD_PASSWORD);
    }

    public String getSupportingServiceCrudDatabase(){
        return propertyResolver.getProperty(SUPPORTING_SERVICE_CRUD_DATABASE);
    }

    public String getCaseEnabled() {
        return propertyResolver.getProperty(CAS_ENABLED);
    }

    public String getServiceLookupDatabase() {
        return propertyResolver.getProperty(SERVICE_LOOKUP_DATABASE);
    }

    public String getEnviornment() {
        return propertyResolver.getProperty(ENVIRONMENT);
    }

    public String getIApisReadUsername() {
        return propertyResolver.getProperty(WS_IAPI_READ_USER_NAME);
    }

    public String getSupportingServicesReadUsername() {
        return propertyResolver.getProperty(WS_SUPPORTINGSERVICES_READ_USER_NAME);
    }

    public String getSupportingServicesReadPassword() {
        return propertyResolver.getProperty(WS_SUPPORTINGSERVICES_READ_USER_NAME_PASSWORD);
    }

    public String getIApisReadPassword() {
        return propertyResolver.getProperty(WS_IAPI_READ_USER_NAME_PASSWORD);
    }
    
    public String getRuntimeEnvironment() {
        return propertyResolver.getProperty("runtimeEnvironment");
    }

    public String getConfigurationManagmentApiVersion() {
        return propertyResolver.getProperty(CONFIGURATION_MANAGEMENT_API_VERSION);
    }

    public String getConfigurationManagmentApiVersionName() {
        return propertyResolver.getProperty(CONFIGURATION_MANAGEMENT_API_VERSION_NAME);
    }

    public String getIdentityApiVersion() {
        return propertyResolver.getProperty(IDENTITY_API_VERSION);
    }

    public String getAppVersion() {
        return propertyResolver.getProperty(APPLICATION_VERSION);
    }

    public String getAppName() {
        String appName = propertyResolver.getProperty(APPLICATION_NAME);
       
		return appName;
    }

    public String getAppMajorVersion() {
        String appVersion = getAppVersion();
        String[] output = appVersion.split("\\.");
        return output[0];
    }

    public String getWebServiceDatabase() {
        String db = propertyResolver.getProperty(WEBSERVICE_DATABASE);
        if (db == null && !getRuntimeEnvironment().equalsIgnoreCase("prod")) {
			db = "v_test";
		}
		return db;
    }

	public String getWS_IAPI_READ_USER_NAME() {
		// TODO Auto-generated method stub
		return WS_IAPI_READ_USER_NAME;
	}

	public String getWS_IAPI_READ_USER_NAME_PASSWORD() {
		// TODO Auto-generated method stub
		return WS_IAPI_READ_USER_NAME_PASSWORD;
	}

    public String getSupportingServicesVersion() {
        if(Strings.isNullOrEmpty(supportingServicesVersion)) {
            supportingServicesVersion = propertyResolver.getProperty(SUPPORTING_SERVICES_VERSION);
        }
        return supportingServicesVersion;
    }
    public String getDocumentManagementAPIVersion() {
        if(Strings.isNullOrEmpty(documentManagementAPIVersion)) {
            documentManagementAPIVersion = propertyResolver.getProperty(DOCUMENT_MANAGEMENT_API_VERSION);
        }
        return documentManagementAPIVersion;
    }

    public String getDocumentServiceUsername() {
        if(Strings.isNullOrEmpty(documentServiceUsername)) {
            documentServiceUsername = propertyResolver.getProperty(DOCUMENT_SERVICE_USERNAME);
        }
        return documentServiceUsername;
    }

    public String getDocumentServicePassword() {
        if(Strings.isNullOrEmpty(documentServicePassword)) {
            documentServicePassword = propertyResolver.getProperty(DOCUMENT_SERVICE_PASSWORD);
        }
        return documentServicePassword;
    }

    public String getDocumentServiceCrudUsername() {
        if(Strings.isNullOrEmpty(documentServiceCrudUsername)) {
            documentServiceCrudUsername = propertyResolver.getProperty(DOCUMENT_SERVICE_CRUD_USERNAME);
        }
        return documentServiceCrudUsername;
    }

    public String getDocumentServiceCrudPassword() {
        if(Strings.isNullOrEmpty(documentServiceCrudPassword)) {
            documentServiceCrudPassword = propertyResolver.getProperty(DOCUMENT_SERVICE_CRUD_PASSWORD);
        }
        return documentServiceCrudPassword;
    }
    
    public String getConfigProperty(String propertyKey) {
        return propertyResolver.getProperty(propertyKey);
    }

    public String getNotificationAPIVersion() {
        if(Strings.isNullOrEmpty(notificationAPIVersion)) {
            notificationAPIVersion = propertyResolver.getProperty(NOTIFICATION_API_VERSION);
        }
        return notificationAPIVersion;
    }

    public String getNotificationCompUsername() {
        if(Strings.isNullOrEmpty(notificationCompUsername)) {
            notificationCompUsername = propertyResolver.getProperty(NOTIFICATION_COMP_USERNAME);
        }
        return notificationCompUsername;
    }

    public String getNotificationCompPassword() {
        if(Strings.isNullOrEmpty(notificationCompPassword)) {
            notificationCompPassword = propertyResolver.getProperty(NOTIFICATION_COMP_PASSWORD);
        }
        return notificationCompPassword;
    }
}
